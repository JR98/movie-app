package com.test.movieapp.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.test.movieapp.models.Movies
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class DataBaseTest: TestCase() {

    private lateinit var db: DataBase
    private lateinit var dao: MovieDao

    @Before
    public override fun setUp() {

        val context = ApplicationProvider.getApplicationContext<Context>()
        val db = Room.inMemoryDatabaseBuilder(context, DataBase::class.java).build()
        dao = db.movieDao()

    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeAndReadMovie() = runBlocking {

        val movie = Movies(1, "/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg",
            "After finding a host body in investigative reporter Eddie Brock",
            "2021-09-30", "Venom: Let There Be Carnage", "en",
            7.2f, 11473.714f)

        dao.insert(listOf(movie))
        val movies = dao.getAll()

        assertTrue(movies.contains(movie))

    }
}