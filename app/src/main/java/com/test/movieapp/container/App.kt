package com.test.movieapp.container

import android.app.Application
import android.view.animation.AlphaAnimation
import com.test.movieapp.dao.DataBase
import com.test.movieapp.di.modules
import com.test.movieapp.di.repositories
import com.test.movieapp.di.retrofitModule
import org.koin.core.context.startKoin

class App: Application() {

    private val appModule = listOf( retrofitModule, repositories, modules )

    override fun onCreate() {
        super.onCreate()

        dbMov = DataBase.getInstance(this)

        startKoin {
            modules(appModule)
        }
    }

    companion object {
        //api_key=09963e300150f9831c46a1828a82a984&language=en-US
        const val apiUrl = "https://api.themoviedb.org/3/"
        const val imgUrl = "https://image.tmdb.org/t/p/w500/"
        const val api_key = "09963e300150f9831c46a1828a82a984"
        const val language = "en-US"

        val buttonClick = AlphaAnimation(1f, 0.6f)

        @get:Synchronized
        lateinit var dbMov: DataBase
    }
}