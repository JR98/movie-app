package com.test.movieapp.container

import com.test.movieapp.vm.MovieViewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class KComponent: KoinComponent {
    val movieViewModel: MovieViewModel by inject()
}