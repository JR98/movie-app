package com.test.movieapp.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.test.movieapp.DetailActivity
import com.test.movieapp.R
import com.test.movieapp.container.App.Companion.buttonClick
import com.test.movieapp.container.App.Companion.imgUrl
import com.test.movieapp.models.Movies
import com.test.movieapp.func.CircularProgress

class MovieAdapter(private val box: List<Movies>, private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_movies, parent, false)
        val vh: RecyclerView.ViewHolder?
        vh = LinearViewHolder(v)
        return vh
    }

    override fun getItemCount(): Int = box.size

    class LinearViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var ivPoster = view.findViewById<ImageView>(R.id.iv_poster)
        internal var tvName = view.findViewById<TextView>(R.id.tv_title)
        internal var tvDesc = view.findViewById<TextView>(R.id.tv_desc)
        internal var rbMov = view.findViewById<RatingBar>(R.id.rb_mov)
    }

    private val circularProgress = CircularProgress

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LinearViewHolder){

            Glide.with(context)
                .load(imgUrl + box[position].poster_path)
                .placeholder(circularProgress.loading(context))
                .centerCrop()
                .into(holder.ivPoster)

            holder.tvName.text = box[position].title

            holder.tvDesc.text = box[position].overview

            holder.rbMov.rating = box[position].vote_average / 2

            holder.itemView.setOnClickListener {
                it.startAnimation(buttonClick)

                val obj = Gson().toJson(box[position])
                context.startActivity(Intent(context, DetailActivity::class.java)
                    .putExtra("movie", obj))
            }
        }
    }
}