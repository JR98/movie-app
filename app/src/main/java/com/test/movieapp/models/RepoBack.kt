package com.test.movieapp.models

import okhttp3.ResponseBody

class RepoBack {

    constructor(code: Int, movies: List<Movies>){
        this.code = code
        this.movies = movies
    }

    constructor(code: Int, movies: List<Movies>, error: ResponseBody?){
        this.code = code
        this.movies = movies
        this.error = error
    }

    var code = 0
    var movies: List<Movies>? = null
    var error: ResponseBody? = null
}