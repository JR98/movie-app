package com.test.movieapp.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movies(
    @PrimaryKey(autoGenerate = true) var id: Int,

    var poster_path: String,

    var overview: String,

    var release_date: String,

    var title: String,

    var original_language: String,

    var vote_average: Float,

    var popularity: Float
)