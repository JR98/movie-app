package com.test.movieapp.models

import com.google.gson.annotations.SerializedName

class Info {
    @SerializedName("total_results")
    var total: Int = 0

    @SerializedName("total_pages")
    var pages: Int = 0

    @SerializedName("results")
    var movies: List<Movies>? = null
}