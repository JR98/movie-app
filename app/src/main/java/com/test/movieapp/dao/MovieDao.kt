package com.test.movieapp.dao

import androidx.room.*
import com.test.movieapp.models.Movies

@Dao
interface MovieDao {

    @Query("SELECT * FROM Movies")
    suspend fun getAll(): List<Movies>

    @Insert
    suspend fun insert(movies: List<Movies>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(movies: List<Movies>)

    /*@Query("SELECT * FROM Movie WHERE id = :id")
    fun findById(id: Int): Movie

    @Query("DELETE FROM Movies")
    fun deleteAll()

    @Delete
    fun delete(movies: Movie)*/
}