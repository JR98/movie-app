package com.test.movieapp.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.movieapp.models.Movies

@Database(entities = [Movies::class], version = 1)
abstract class DataBase: RoomDatabase() {
    abstract fun movieDao(): MovieDao

    companion object {

        private var INSTANCE: DataBase? = null

        fun getInstance(context: Context): DataBase {
            if (INSTANCE == null) {

                INSTANCE = Room.databaseBuilder(
                    context,
                    DataBase::class.java,
                    "movie-db")
                    .allowMainThreadQueries()
                    .build()
            }

            return INSTANCE!!
        }
    }
}