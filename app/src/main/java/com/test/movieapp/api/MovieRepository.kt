package com.test.movieapp.api

import android.util.Log
import com.google.gson.Gson
import com.test.movieapp.dao.DataBase
import com.test.movieapp.dao.MovieDao
import com.test.movieapp.models.RepoBack
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieRepository(private val apiInterface: ApiInterface) {

    suspend fun doGetMovies(daoMov: MovieDao, apiKey: String, language: String) = withContext(Dispatchers.IO) {
        try {
            val response = apiInterface.doGetListResources(apiKey, language)

            if (response.isSuccessful) {
                val success = response.body()!!.movies!!
                val dataDao = daoMov.getAll()

                if (dataDao.isNullOrEmpty()) daoMov.insert(success)
                else daoMov.update(success)

                RepoBack(response.code(), success)
            } else {
                val error = RepoBack(response.code(), daoMov.getAll(), response.errorBody())
                error
            }
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
            RepoBack(400, daoMov.getAll())
        }
    }

}