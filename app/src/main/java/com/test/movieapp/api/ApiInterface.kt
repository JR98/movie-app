package com.test.movieapp.api

import com.test.movieapp.models.Info
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("movie/popular?")
    suspend fun doGetListResources(@Query("api_key") apiKey: String,
                                   @Query("language") language: String): Response<Info?>

}