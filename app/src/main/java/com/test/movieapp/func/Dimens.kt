package com.test.movieapp.func

import android.util.DisplayMetrics
import androidx.fragment.app.FragmentActivity
import kotlin.math.min


object Dimens {
    fun getDimen(context: FragmentActivity): Float {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getRealMetrics(metrics)
        val widthPixeles = metrics.widthPixels
        val heightPixeles = metrics.heightPixels
        val scaleFactor = metrics.density

        val widthDp = widthPixeles / scaleFactor
        val heightDp = heightPixeles / scaleFactor

        return min(widthDp, heightDp)
    }
}