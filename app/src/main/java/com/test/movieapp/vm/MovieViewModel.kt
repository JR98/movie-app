package com.test.movieapp.vm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.movieapp.api.MovieRepository
import com.test.movieapp.container.App
import com.test.movieapp.models.Info
import com.test.movieapp.models.RepoBack
import kotlinx.coroutines.launch

class MovieViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val dbM = App.dbMov.movieDao()
    private val _info = MutableLiveData<RepoBack>()
    val info: LiveData<RepoBack> get() = _info

    fun fetchMovies(connect: Boolean, key: String, language: String) = viewModelScope.launch {
        val response = if (connect)  {
            movieRepository.doGetMovies(dbM, key, language)
        } else RepoBack(400, dbM.getAll())

        _info.value = response
    }
}