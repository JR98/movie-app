package com.test.movieapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.jcodecraeer.xrecyclerview.ProgressStyle
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.test.movieapp.adapters.MovieAdapter
import com.test.movieapp.container.App
import com.test.movieapp.container.KComponent
import com.test.movieapp.dao.DataBase
import com.test.movieapp.func.Dimens
import com.test.movieapp.models.Movies
import android.net.NetworkInfo

import android.net.ConnectivityManager
import com.test.movieapp.container.App.Companion.api_key
import com.test.movieapp.container.App.Companion.language


class MainActivity : AppCompatActivity() {

    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var recyclerView: XRecyclerView
    private var adapterMovie: MovieAdapter? = null

    private val kComponent = KComponent()

    private var listMovies: ArrayList<Movies> = ArrayList()

    lateinit var contextView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contextView = findViewById(R.id.main_view)
        val dimen = Dimens.getDimen(this)

        //instance recyclerView & gridLayout
        recyclerView = findViewById(R.id.Xrecyclerview)

        //switch dimens for tablet or smartphone
        gridLayoutManager = GridLayoutManager(this, if (dimen > 600) 3 else 1)
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.setHasFixedSize(true)

        //retrofit and koin with coroutines
        kComponent.movieViewModel.info.observe(this, { data ->
            listMovies.addAll(data.movies!!)

            if (data.code == 200) {
                if (!data.movies.isNullOrEmpty()) {
                    addAdap()
                    recyclerView.refreshComplete()
                } else snackB("No more items")


            } else {
                if (listMovies.size > 0) {
                    addAdap()
                }
                recyclerView.refreshComplete()
                snackB("Error, try later")
            }
        })

        //Xrecyclerview refresh
        recyclerView.setLoadingMoreEnabled(false)
        recyclerView.setRefreshProgressStyle(ProgressStyle.SemiCircleSpin)
        recyclerView.setLoadingListener(object : XRecyclerView.LoadingListener{
            override fun onRefresh() {
                listMovies.clear()
                loadMovies()
            }

            override fun onLoadMore() {
                //TODO
            }
        })

        loadMovies()
    }

    private fun addAdap() {
        adapterMovie = MovieAdapter(listMovies, this)
        recyclerView.adapter = adapterMovie!!
    }

    private fun loadMovies() {
        if (!isNetworkAvailable()) snackB(
            getString(R.string.lost_connection)
        )
        kComponent.movieViewModel.fetchMovies(isNetworkAvailable(), api_key, language)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun snackB(message: String){
        Snackbar.make(contextView, message, Snackbar.LENGTH_SHORT).show()
    }
}