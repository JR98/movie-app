package com.test.movieapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.test.movieapp.container.App.Companion.imgUrl
import com.test.movieapp.func.CircularProgress
import com.test.movieapp.models.Movies

class DetailActivity : AppCompatActivity() {

    private val circularProgress = CircularProgress

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val toolbar = findViewById<Toolbar>(R.id.toolbarDetail)
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_ios_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        //get intent movie data
        val movie = intent.extras!!.getString("movie")
        val detail = Gson().fromJson(movie.toString(), Movies::class.java)

        val dtImg = findViewById<ImageView>(R.id.dt_poster)
        val dtName = findViewById<TextView>(R.id.dt_title)
        val dtOverview = findViewById<TextView>(R.id.dt_overview)
        val dtRating = findViewById<RatingBar>(R.id.dt_rating)
        val dtDate = findViewById<TextView>(R.id.dt_release_date)
        val dtPopu = findViewById<TextView>(R.id.dt_popularity)

        Glide.with(this).load(imgUrl + detail.poster_path)
            .placeholder(circularProgress.loading(this))
            .centerCrop().into(dtImg)

        dtName.text = detail.title
        dtOverview.text = detail.overview
        dtRating.rating = detail.vote_average / 2
        dtDate.text = detail.release_date
        dtPopu.text = "viewed: ${detail.popularity}"
    }
}