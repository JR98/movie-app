package com.test.movieapp.di

import com.test.movieapp.api.MovieRepository
import com.test.movieapp.dao.MovieDao
import org.koin.dsl.module

val repositories = module {
    factory { MovieRepository( get() ) }
}