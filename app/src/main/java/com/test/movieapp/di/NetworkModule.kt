package com.test.movieapp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.test.movieapp.api.ApiInterface
import com.test.movieapp.container.App.Companion.apiUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitModule  = module {

    factory<Gson> { GsonBuilder().create() }
    factory {
        OkHttpClient.Builder()
            .addInterceptor {
                val newReq: Request = it.request().newBuilder().build()
                it.proceed(newReq)
            }
            .build()
    }

    factory<Retrofit> {
        Retrofit.Builder()
            .client(get())
            .baseUrl(apiUrl)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }
    factory { get<Retrofit>().create(ApiInterface::class.java) }
}