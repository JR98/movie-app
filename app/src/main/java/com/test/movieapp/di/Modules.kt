package com.test.movieapp.di

import com.test.movieapp.vm.MovieViewModel
import org.koin.dsl.module

val modules = module {
    factory { MovieViewModel( get() ) }
}